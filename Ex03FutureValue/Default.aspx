﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Harris HW03 Future Value</title>
</head>
<body>
    <header>
        <img id="logo" alt="Murach logo" src="Images/MurachLogo.jpg"/>
        <link href="Styles.css" rel="stylesheet" />
    </header>
    <section>
        <form id="form1" runat="server">
            <h1>401K Future Value Calculator</h1>
            <label>Monthly investment:</label>
            <asp:DropDownList ID="ddlMonthlyInvestment" runat="server" CssClass="entry"></asp:DropDownList><br />
            <label>Annual interest rate:</label>
            <asp:TextBox ID="txtInterestRate" runat="server" CssClass="entry">6.0</asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvInterestRate" runat="server" ErrorMessage="Interest rate is required." ControlToValidate="txtInterestRate" CssClass="validators"></asp:RequiredFieldValidator><br />
            <label>Number of years:</label>
            <asp:TextBox ID="txtYears" runat="server" CssClass="entry">10</asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvYears" runat="server" ErrorMessage="Number of years is required." ControlToValidate="txtYears" CssClass="validators"></asp:RequiredFieldValidator><br />
            <label>Future value:</label>
            <asp:Label ID="lblFutureValue" runat="server" Text=""></asp:Label><br />
            <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="button" 
                onclick="btnCalculate_Click"/>
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="button" 
                onclick="btnClear_Click" CausesValidation="False" />
        </form>
    </section>    
</body>
</html>
